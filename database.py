from peewee import *
from config import get_config_from_file
from flask_login import UserMixin

c = get_config_from_file()["database"]
db = MySQLDatabase(c["database"], user=c["user"], password=c["password"],
                         host=c["host"], port=int(c["port"]), autoconnect=False)


class User(Model, UserMixin):
    id = IntegerField()
    email = CharField()
    firstname = CharField()
    lastname = CharField()
    organization = CharField()
    role = CharField()
    password = CharField()

    class Meta:
        database = db

    def get_id(self):
        return str(self.id)
