import {IfcViewerAPI} from 'web-ifc-viewer';

const container = document.getElementById('viewer-container');
export const viewer = new IfcViewerAPI({container});
viewer.addAxes();
viewer.addGrid();
viewer.IFC.setWasmPath('/static/3dviewer/wasm/');
viewer.IFC.applyWebIfcConfig({
    COORDINATE_TO_ORIGIN: true
});

let ifc_url = "{{ifc_url}}" ;
viewer.IFC.loadIfcUrl(ifc_url, true)
    .then(() => {
    });


async function handleClick() {
    let {modelID, id} = await viewer.IFC.pickIfcItem();
    let properties = await viewer.IFC.getProperties(modelID, id, true);
    let elem = document.getElementById("picked-item-details");
    elem.innerHTML = "<pre>" + properties.constructor.name + "\n" + JSON.stringify(properties, null, 4) + "</pre>";
}

window.onmousemove = viewer.IFC.prePickIfcItem;
window.onclick = handleClick;

