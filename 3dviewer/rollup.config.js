import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';

export default {
  input: 'main.js',
  output: {
    file: "../templates/js/3dviewer.js",
    format: 'iife',
    name: "viewermodule"
  },
  plugins: [ resolve(), commonjs() ]
};